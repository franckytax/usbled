/*
 * Copyright (c) 2019 Denis Bodor <dbodor@rollmops.ninja>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

//  /usr/src/sys/dev/usb/moscom.c


#include <sys/param.h>
#include <sys/systm.h>
#include <dev/usb/usb.h>
#include <dev/usb/usbdi.h>

#define USBLED_VID		0x2e8a
#define USBLED_PID		0x0003
#define USBLED_IFACE_INDEX	0
#define CMD_SET_LED		8

// my data
struct usbled_softc {
        struct device		sc_dev;
	struct usbd_device	*sc_udev;
	int			sc_opened;
};

struct cfdriver usbled_cd = {
	NULL,		// devices found
	"usbled",	// device name
	DV_DULL		// device classification -> generic, no special info
};

const struct usb_devno usbled_devs[] = {
	{ USBLED_VID, USBLED_PID },
};


int usbled_match(struct device *, void *, void *);
void usbled_attach(struct device *, struct device *, void *);
int usbled_detach(struct device *, int);
//static int usb_read(struct usbled_softc *, int, int, int, void *, int);
static int usb_write(struct usbled_softc *, int, int, int, void *, int);

// configuration attachment and driver
const struct cfattach usbled_ca = {
	sizeof (struct usbled_softc),	// size of dev data (for malloc)
	usbled_match,			// returns a match level
	usbled_attach,
	usbled_detach,
	NULL
};

int
usbled_match(struct device *parent, void *match, void *aux)
{
	struct usb_attach_arg *uaa = aux;
	int ret = UMATCH_NONE;

	if (uaa->iface == NULL)
		return (UMATCH_NONE);

	if (usb_lookup(usbled_devs, uaa->vendor, uaa->product) != NULL) {
		ret = UMATCH_VENDOR_PRODUCT;
		printf(">>> usbled match\n");
	}

	return (ret);
}

void
usbled_attach(struct device *parent, struct device *self, void *aux)
{
	struct usbled_softc *sc = (struct usbled_softc *)self;
	struct usb_attach_arg *uaa = aux;
	struct usbd_device *dev = uaa->device;

	printf(">>> usbled attach\n");
	sc->sc_udev = dev;
	return;
}

int
usbled_detach(struct device *self, int flags)
{
	//struct usbled_softc *sc = (struct usbled_softc *)self;
	int ret = 0;

	printf(">>> usbled detach\n");

	return (ret);
}

int
usbledopen(dev_t dev, int flag, int mode, struct proc *p)
{
	struct usbled_softc *sc;
	int ret = 0;

	sc = (struct usbled_softc *)device_lookup(&usbled_cd, minor(dev));

	if (sc == NULL)
		return (ENXIO);

	if (sc->sc_opened)
		ret = EBUSY;
	else
		sc->sc_opened = 1;

	printf(">>> usbled%u open\n", minor(dev));

	if ((ret = usb_write(sc, CMD_SET_LED, 1, 0, NULL, 0)) < 0) {
		printf(">>> usbledopen usb_write error!\n");
	}

	return (ret);
}

int
usbledclose(dev_t dev, int flag, int mode, struct proc *p)
{
	struct usbled_softc *sc;
	int ret = 0;

	sc = (struct usbled_softc *)device_lookup(&usbled_cd, minor(dev));

	if (sc == NULL)
		return (ENXIO);

	sc->sc_opened = 0;

	printf(">>> usbled%u close\n", minor(dev));

	if ((ret = usb_write(sc, CMD_SET_LED, 0, 0, NULL, 0)) < 0) {
		printf(">>> usbledclose usb_write error!\n");
	}

	return (ret);
}

static int
usb_write(struct usbled_softc *sc, int cmd, int value, int index, void *data, int len)
{
	int error;
	struct usb_device_request req;
	int actlen;

	req.bmRequestType = UT_WRITE_VENDOR_INTERFACE;
	req.bRequest = cmd;
	USETW(req.wValue, value);
	USETW(req.wIndex, index);
	USETW(req.wLength, len);

	error = usbd_do_request_flags(sc->sc_udev, &req, data, 0, &actlen, 2000);

	if (error)
		actlen = -1;

	return (actlen);
}
