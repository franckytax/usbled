# usbled : a USB device driver for OpenBSD

## Introduction

The goal of this project is to code a USB device driver for the Raspberry Pi pico in the OpenBSD kernel 7.5. This code has been only tested with the amd64 architecture.

This code is based on the article about how to code a driver for OpenBSD in the Linux magazine N°269, written by Denis Bodor.

## Compilation of the custom kernel on OpenBSD for amd64 architecture

1. Generate the compilation direcory

        cd src/sys/arch/amd64/conf
        config CUSTOM.MP

2. Start the kernel compilation, replace X with the number of cpu cores you have. If you don't known, run the command : sysctl hw.ncpu

        cd ../compile/CUSTOM.MP
        make -jX

3. At the end of the compilation, the generated kernel will be found at this location :
    
        src/sys/arch/amd64/compile/CUSTOM.MP/obj/bsd



